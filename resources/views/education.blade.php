@extends('layouts.main')

@section('container')
<section class="resume-section" id="education">
    <div class="resume-section-content">
        <h2 class="mb-5">Education</h2>
        <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
            <div class="flex-grow-1">
                <h3 class="mb-0">SMA N 4 SINGARAJA</h3>
                <div class="subheading mb-3">Science and English </div>
                <div>Mipa 1 Bhs Inggris</div>
                <p>GPA: 3.17</p>
            </div>
            <div class="flex-shrink-0"><span class="text-primary">August 2016 - May 2019</span></div>
        </div>
        <div class="d-flex flex-column flex-md-row justify-content-between">
            <div class="flex-grow-1">
                <h3 class="mb-0">Universitas Pendidikan Ganesha</h3>
                <div class="subheading mb-3">Sistem Informasi</div>
                <div>Semester 5</div>
                <p>GPA: 3.56</p>
            </div>
            <div class="flex-shrink-0"><span class="text-primary">August 2019</span></div>
        </div>
    </div>
</section>
@endsection