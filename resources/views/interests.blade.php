@extends('layouts.main')

@section('container')
<section class="resume-section" id="interests">
    <div class="resume-section-content">
        <h2 class="mb-5">Interests</h2>
        <p>selain dari apa yang saya kerjakan sekarang, saya memiliki hobi seperti bermain game, jalan-jalan, dan berolahraga.</p>
        <p class="mb-0">Ketika dipaksa di dalam ruangan, saya mengikuti sejumlah film dan acara televisi bergenre fiksi ilmiah dan fantasi, saya adalah calon koki, dan saya menghabiskan banyak waktu luang saya menjelajahi kemajuan teknologi terbaru di dunia pengembangan web front-end.</p>
    </div>
</section>
@endsection