<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\rumahCont;
use App\Http\Controllers\edukasiCont;
use App\Http\Controllers\expreCont;
use App\Http\Controllers\interCont;
use App\Http\Controllers\skillCont;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [rumahCont::class,'index']);

Route::get('/experience', [expreCont::class,'indexexpre']);

Route::get('/education', [edukasiCont::class,'indexedu']);

Route::get('/interests', [interCont::class,'indexinter']);

Route::get('/skills', [skillCont::class,'indexskill']);


